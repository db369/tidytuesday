---
title: "Password Strength"
output: html_notebook
---

```{r}
library(tidyverse)

```

```{r theme_tweaks}

my_font <- "Comfortaa"
#my_bkgd <- "#f5f5f2"
my_bkgd <- "grey20"
text_color <- "grey90"
my_theme <- theme(text = element_text(family = my_font, color = text_color),
                  rect = element_rect(fill = my_bkgd),
                  plot.background = element_rect(fill = my_bkgd, color = NA),
                  panel.background = element_rect(fill = my_bkgd, color = NA),
                  panel.border = element_blank(),
                  legend.background = element_rect(fill = my_bkgd, color = NA),
                  legend.key = element_rect(fill = my_bkgd),
                  plot.caption = element_text(size = 6),
                  axis.text = element_text(family = my_font, color = text_color),
                  axis.ticks = element_blank(),
                  strip.background = element_blank(),
                  strip.text = element_text(family = my_font, color = "grey", 
                                            hjust = 0, size = rel(1.1)))

theme_set(theme_light() + my_theme)


```

```{r}
passwords <- readr::read_csv('https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2020/2020-01-14/passwords.csv')
passwords
```

```{r}

# https://en.wikipedia.org/wiki/Password_strength
bits_of_entropy <- function(pword) {
  if (!is.na(pword)) {
    ln <- nchar(pword)
    entropy = 0
    i = 1
    
    while (i <= ln){
      entropy = entropy + 
        case_when(i == 1 ~ 4,
                  i > 1 & i <= 8 ~ 2,
                  i > 8 & i <= 20 ~ 1.5,
                  TRUE ~ 1)
      i = i + 1
    }
    
    if (str_detect(pword,"[A-Z]") * str_detect(pword,"[a-z]") * str_detect(pword,"[0-9]")){
      entropy = entropy + 6
    }
    return(entropy)
  }
}

pw <- passwords %>% 
  filter(!is.na(password)) %>%
  mutate(online_crack_sec = case_when(time_unit == "years" ~ value * 365 / (24 * 60 * 60),
                                      time_unit == "months" ~ value * 30 / (24 * 60 * 60),
                                      time_unit == "weeks" ~ value * 7 / (24 * 60 * 60),
                                      time_unit == "days" ~ value / (24 * 60 * 60),
                                      time_unit == "hours" ~ value / (60 * 60),
                                      time_unit == "minutes" ~ value / 60,
                                      TRUE ~ value))

pw$entropy = sapply(pw$password, bits_of_entropy)

```

```{r}

summary(pw)

pw %>%
  #group_by(category) %>%
  #summarize(med_offline = mean(offline_crack_sec,na.rm=T), 
  #          med_online = mean(online_crack_sec,na.rm=T), 
  #          med_ent = mean(entropy, na.rm=T)) %>%
  ggplot(aes(offline_crack_sec,entropy,col=category)) +
  geom_segment(aes(xend=offline_crack_sec, y=entropy, yend=entropy), x=0) +
  geom_point(size=3, alpha = 0.6) +
  facet_wrap(~category, scales = 'free_x')

```

```{r}

pw %>%
  ggplot(aes(category, entropy, fill=category)) +
  geom_violin(alpha = 0.6, col = my_bkgd, show.legend = F) +
  coord_flip() +
  scale_y_continuous(breaks = seq(10,20,2)) +
  labs(title = "Password Strength By Category",
       subtitle = "Average entropy = 14 across all categories",
       x = "", y = "Bits of Entropy\n(Based on NIST Special Publication 800-63-2)",
       caption = "Soures: Information is Beautiful, Wikipedia  |  By @DaveBloom11") +
  theme(panel.grid = element_line(color="gray30"),
        panel.border = element_rect(color="grey30", fill = NA))

ggsave("password_strength.png")

```

```{r}

pw %>% 
  ggplot(aes(rank,entropy, col=category)) +
  geom_jitter(size = 2)


```


```{r}

pw %>%
  group_by(category) %>%
  filter(!is.na(category)) %>%
  summarise(avg_off = mean(offline_crack_sec, na.rm=T), avg_on = mean(online_crack_sec, na.rm=T),
            med_off = median(offline_crack_sec, na.rm=T), med_on = median(online_crack_sec, na.rm=T)) %>%
  mutate(diff_avg = abs(avg_off-avg_on), diff_med = abs(med_off-med_on),
         category = fct_reorder(category, diff_avg)) %>%
  ggplot(aes(category,avg_off)) +
  geom_segment(aes(x = category, xend = category, y = avg_off, yend=avg_on), size = 0.5, linetype = 3) +
  geom_point(col = 'blue', size=3) +
  geom_point(aes(y=avg_on), col='red', size=3) +
  coord_flip()


```
```{r}

pw %>%
  group_by(category) %>%
  filter(!is.na(category)) %>%
  summarise(avg_off = mean(offline_crack_sec, na.rm=T), avg_on = mean(online_crack_sec, na.rm=T),
            med_off = median(offline_crack_sec, na.rm=T), med_on = median(online_crack_sec, na.rm=T)) %>%
  mutate(diff_avg = abs(avg_off-avg_on), diff_med = abs(med_off-med_on),
         category = fct_reorder(category, diff_med)) %>%
  ggplot(aes(category,med_off)) +
  geom_segment(aes(x = category, xend = category, y = med_off, yend=med_on), size = 0.5, linetype = 3) +
  geom_point(col = 'blue', size=3) +
  geom_point(aes(y=med_on), col='red', size=3) +
  coord_flip()


```

```{r}
pw %>% 
  count(category, sort=T)

```

```{r}

summary(pw)

```

