# Tidy Tuesday 2020 - Week #2
### Australia Fires
See https://github.com/rfordatascience/tidytuesday for more information.

For this #TidyTuesday, I compared average monthly temperatures before / including 1964 to uncover some evidence of generally increasing temperatures. I also had to remind myself that Australian winter takes place during the summer in the northern hemisphere. :)


![](avg_monthly_temps.png)
## Code Snippet
```r

library(tidyverse)
library(ghibli)
library(lubridate)

temperature <- readr::read_csv('https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2020/2020-01-07/temperature.csv')

temp <- temperature %>%
  mutate(year = year(date),
         month = month(date))

chart_data <- temp %>%
  mutate(timing = ifelse( year <= 1964, 0, 1)) %>%
  group_by(month, timing) %>%
  summarize(avg_temp = mean(temperature, na.rm = T)) 

chart_data %>%
  ggplot(aes(month(month, abbr = T, label = T), avg_temp, group=timing, col=factor(timing))) +
  geom_line(size=2) + geom_point(size=4, shape=21, fill=my_bkgd) +
  scale_color_manual(name = "", labels = c("Before 1964", "After 1964"), values = ghibli_palettes$PonyoMedium) +
  labs(title = "Average Monthly Temperatures Before and After 1964",
       subtitle = "Australia, 1910 to 2019",
       y = "Average Temperature (°C)",
       x = "Month",
       caption = "Source: Australian Bureau of Meterology  |  By @DaveBloom11") +
  theme(legend.position = c(0.8,1),
        legend.direction = "horizontal")


```


