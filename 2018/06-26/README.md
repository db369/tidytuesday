
# Tidy Tuesday - Week 13
### Alcohol Consumption

My contribution to week 13 of TidyTuesday. I had a few ideas for this one, but only one worked out.

See https://github.com/rfordatascience/tidytuesday for more information.

#### Alcohol Consumption by Region
![](bev.png)


#### Code
![code](carbon.png)