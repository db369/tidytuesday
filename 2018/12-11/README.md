# Tidy Tuesday - Week 37
### NYC Restaurant Inspections

Well, this was a lot more interesting before I realized that I didn't filter out duplicate restaurants. So filtering out all restaurants but the last inspection date, I determined the most common cuisines found in each borough and the distribution of grades for each cuisine, which were mostly 'A' after filtering. 

See https://github.com/rfordatascience/tidytuesday for more information.

## Common restaurant cuisines in each borough
![](common.png)

## Code Snippet
```r
library(tidyverse)
library(ghibli)
library(gridExtra)

my_font <- "Comfortaa"
my_bkgd <- "#f5f5f2"
my_theme <- theme(text = element_text(family = my_font, color = "#22211d"),
                  rect = element_rect(fill = my_bkgd), 
                  plot.background = element_rect(fill = my_bkgd, color = NA), 
                  panel.background = element_rect(fill = my_bkgd, color = NA), 
                  panel.border = element_blank(),
                  legend.background = element_rect(fill = my_bkgd, color = NA),
                  legend.key = element_rect(fill = my_bkgd),
                  plot.caption = element_text(size = 6))

theme_set(theme_light() + my_theme)

df <- read_csv("https://data.cityofnewyork.us/api/views/43nn-pn8j/rows.csv")

# Cleaning names with janitor and dropping some variables
# note that I chose not to sample -- it was only 80K more rows
df <- df %>% 
      janitor::clean_names() %>%
      select(-phone, -grade_date, -record_date, -building, -street)

# Cleaned up some cuisine descriptions manually
df$cuisine_description[str_detect(df$cuisine_description,"Coffee/Tea")] <- "Café/Coffee/Tea"
df$cuisine_description[str_detect(df$cuisine_description, "Latin")] <- "Latin"
df$cuisine_description[str_detect(df$cuisine_description, "Bottled")] <- "Bottled Beverages"

# Filtered duplicate restaurants and dropped a few more variables
df_filtered <- df %>%
  select(-violation_code,-violation_description) %>%
  group_by(camis) %>%
  mutate(the_rank  = rank(inspection_date, ties.method = "random")) %>%
  filter(the_rank == 1) %>% select(-the_rank)


# function to create a top 10 list
top_10_list <- function(x){
  temp <- df_filtered %>%
  filter(boro == x) %>%
  group_by(cuisine_description, grade)  %>%
  summarize(n = n()) %>%
  spread(grade,3) %>%
  ungroup()

  totals <- rowSums(temp[2:4], na.rm=T)

  cbind(temp,total = totals) %>%
  arrange(desc(total)) %>%
  head(10) %>%
  gather("grade","n", 2:4)
  
}

# function to plot the top_10_list
top_10_plot_alt <- function(x, title, leg = F) {
  x %>%
  mutate(cuisine_description = fct_reorder(cuisine_description, total)) %>%
  ggplot(aes(cuisine_description,n,fill=grade)) + 
  geom_col() +
  coord_flip() +
  scale_fill_manual(values = ghibli_palettes$MononokeLight, name = "Grade") +
  labs(x="",y="",title = title) +
  theme(legend.position = ifelse(leg == T,"right","none"))
}

# Build top 10 lists for each borough
queens <- top_10_list("QUEENS")
brooklyn <- top_10_list("BROOKLYN")
si <- top_10_list("STATEN ISLAND")
nyc <- top_10_list("MANHATTAN")
bronx <- top_10_list("BRONX")

# Build plots for each borough
queens_plot <- top_10_plot_alt(queens, "Queens")
brooklyn_plot <- top_10_plot_alt(brooklyn, "Brooklyn")
si_plot <- top_10_plot_alt(si, "Staten Island")
nyc_plot <- top_10_plot_alt(nyc, "Manhattan")
bronx_plot <- top_10_plot_alt(bronx, "Bronx")

# Prepare a title for the graphic
title_grob <- ggplot() +
  annotate("text",
           x=0,y=0, size = 6, hjust=0.5, vjust=0.5, family=my_font, 
           label = "Most Common Restaurant Cuisines\nBy Borough And Grade") +
  my_theme +
  theme(panel.grid = element_blank(),
        axis.text = element_blank(),
        axis.title = element_blank())

# Prepare a caption for the graphic
src_grob <- ggplot() + 
    annotate("text", x=0, y=0, size = 3, hjust=0.5, vjust=0.5, family=my_font, 
           label = "Source: New York Open Data Portal") +
  my_theme +
  theme(panel.grid = element_blank(),
        axis.text = element_blank(),
        axis.title = element_blank())

# Grab legend from a chart
plot_leg <- grid.arrange(cowplot::get_legend(top_10_plot_alt(nyc, "temp", T)))

# Layout matrix
ml <- rbind(c(1,1),
            c(2,3),
            c(4,5),
            c(6,7),
            c(NA,8))
            
# heights of each row in the matrix
heights <- c(12,30,30,30,4)

# final graphic
g <- grid.arrange(title_grob, bronx_plot, brooklyn_plot, nyc_plot, queens_plot, si_plot, plot_leg, src_grob,
                  layout_matrix = ml, heights = heights) 


```