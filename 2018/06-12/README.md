
# Tidy Tuesday - Week 11
### FIFA World Cup Audience

My contribution to week 11 of TidyTuesday. I thought I'd play around with the Leaflet package and started with The R Graph Gallery's example (https://www.r-graph-gallery.com/183-choropleth-map-with-leaflet/). 

See https://github.com/rfordatascience/tidytuesday for more information.

#### Leaflet Map 
_(note that running m.html in your browser will allow you to mouseover for detail)_
![leaflet_map](map.png)

#### Code
![code](carbon.png)