# Tidy Tuesday - Week 35
### Maryland Bridges

For this dataset, I tried to identify bridges with a poor rating and the most traffic. Then I colored by the responsible authority to get a sense of how that may be distributed.

See https://github.com/rfordatascience/tidytuesday for more information.


![](map.png)
```r

library(tidyverse)

my_font <- "Comfortaa"
my_theme <- theme(text = element_text(family = my_font, color = "#22211d"),
                  rect = element_rect(fill = "#f5f5f2", color = NA), 
                  plot.background = element_rect(fill = "#f5f5f2", color = NA), 
                  panel.background = element_rect(fill = "#f5f5f2", color = NA), 
                  panel.border = element_blank(),
                  legend.background = element_rect(fill = "#f5f5f2", color = NA),
                  legend.key = element_rect(fill = "#f5f5f2"),
                  plot.caption = element_text(size = 6))

theme_set(theme_light() + my_theme)

bridges %>%
  filter(bridge_condition == "Poor",
         avg_daily_traffic > 5000) %>%
  ggplot(aes(long,lat, size = avg_daily_traffic, fill = responsibility)) +
  borders("state",regions="Maryland", fill = "gray40") +
  geom_point(shape=21, alpha = 0.7, col = "#f5f5f2") +
  scale_fill_viridis_d(option = "A") +
  labs(title = "Maryland Bridges With A \"Poor\" Rating",
       subtitle = "By Responsible Authority and Average Daily Traffic",
       fill = "Responsible Authority",
       size = "Avg Daily\nTraffic",
       x="",y="",
       caption = "Source: Baltimore Sun Data Desk") +
  theme(text = element_text(color = "#f5f5f2"),
        legend.justification=c(0,0), 
        legend.position=c(0.025,0.025),
        plot.background = element_rect(fill="gray20"),
        panel.background = element_rect(fill="gray30", color = "gray50"),
        legend.background = element_rect(fill = "gray20", color = "gray50"),
        legend.key = element_blank(),
        axis.text = element_blank(),
        axis.ticks = element_blank())
        
```
