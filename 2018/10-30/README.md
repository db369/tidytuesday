# Tidy Tuesday - Week 31
### R and Package download stats

The charts speak for themselves this week...

See https://github.com/rfordatascience/tidytuesday for more information.

#### R Downloads By Country
![](by_country.png)

#### R Download Frequncy By Month
![](by_month.png)


#### Growth of Installs By Operating System
![](os_growth.png)