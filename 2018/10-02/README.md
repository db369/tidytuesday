
# Tidy Tuesday - Week 27
### US Births

My contribution to week 27 of TidyTuesday is a jitter chart by day of week. I highlighted fridays that fell on the 13th of the month, but more interesting was that there are fewer births on weekends than weekdays.

See https://github.com/rfordatascience/tidytuesday for more information.

#### US Births by Day of Week
![](us_births.png)

