
# Tidy Tuesday - Week 26
### Global threat to agriculture from invasive species

It's been a while, but needed to get back into this. My contribution to week 26 of TidyTuesday is a simple scatter chart of Invasion Cost versus Mean GDP and highlights a positive relationship between the two factors after log normalizing the data.

See https://github.com/rfordatascience/tidytuesday for more information.

#### Invasive Species in Africa -- star points indicate the max and min GDP proportions
![](InvCost_GDPM.png)

#### Code
![](carbon.png)
