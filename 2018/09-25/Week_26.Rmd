---
title: "Invasive Species in Africa"
output: html_notebook
---

```{r}
library(tidyverse)

```

```{r}
my_theme <- theme(text = element_text(family = "Comfortaa", color = "#22211d"),
                  rect = element_rect(fill = "#f5f5f2", color = NA), 
                  plot.background = element_rect(fill = "#f5f5f2", color = NA), 
                  panel.background = element_rect(fill = "#f5f5f2", color = NA), 
                  legend.background = element_rect(fill = "#f5f5f2", color = NA),
                  legend.key = element_rect(fill = "#f5f5f2"),
                  plot.caption = element_text(size = 6))


```

```{r}
path <- "https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2018-09-25/"
files <- list("africa_species","table_1","table_2","table_3","table_4","table_6")

for (file in files){
    assign(file, read_csv(paste0(path, file, ".csv")))
}
  

```

```{r}
head(africa_species)
head(table_1)
head(table_2)
head(table_3)
head(table_4)
head(table_6)
```

```{r}

max_ratio <- table_3 %>%
  top_n(1, gdp_proportion)

min_ratio <- table_3 %>%
  top_n(-1, gdp_proportion)

table_3 %>%
  mutate(label = case_when(country == max_ratio$country ~ paste0("Max GDP Proportion:\n",country," (", gdp_proportion,")"),
                           country == min_ratio$country ~ paste0("Min GDP Proportion:\n",country," (", gdp_proportion,")"))) %>%
  ggplot(aes(x=gdp_mean, y=invasion_cost, color=gdp_proportion)) + 
    geom_point() + 
    geom_point(data=max_ratio, aes(x=gdp_mean, y=invasion_cost), shape=8, size = 3) +
    geom_point(data=min_ratio, aes(x=gdp_mean, y=invasion_cost), shape=8, size = 3) +
    geom_smooth(color = "darkslategrey", alpha=0.15) +
    geom_label(aes(label=label), nudge_x = 0, nudge_y = 0.6, alpha=0.6) +
    scale_x_log10() +
    scale_y_log10() + 
    scale_color_gradient(high = "yellowgreen", low = "darkolivegreen", name = "") +
    theme_minimal() + my_theme +
    theme(legend.key.height = unit(18, "mm"), 
          legend.key.width = unit(3, "mm"),
          legend.box.just = "top") +
    labs(title = "Invasive Species in Africa",
         subtitle = "Invasion Cost vs Mean GDP", 
         x = "Mean GDP", y = "Invasion Cost",
         caption = "Source: Paini et al, 2016 report on \"Global threat to agriculture from invasive species\"") 

ggsave("InvCost_GDPM.png")

```

