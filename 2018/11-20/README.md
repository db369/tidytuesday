# Tidy Tuesday - Week 34
### Transgender Day of Remembrance

Sadly, it seems the Americas are not very tolerant of transgender people, Brazil in particular. 

See https://github.com/rfordatascience/tidytuesday for more information.

#### Total Deaths by Country
66% of deaths were in Brazil, Mexico, and USA
![](country.png)
```r
tdor %>%
  filter(!is.na(`Cause of death`)) %>%
  select(Country, `Cause of death`) %>%
  mutate(reported = (`Cause of death` != "not reported"),
         Country = fct_rev(fct_infreq(Country))) %>%
  ggplot(aes(fct_relevel(fct_lump(Country,10), "Other"), fill=reported)) +
  geom_bar(position = "stack") +
  scale_fill_manual(values = ghibli_palettes$MononokeLight, name = "Reported") +
  coord_flip() +
  labs(title = "Number of Transgender Deaths - Both Reported and Unreported",
       subtitle = "By Country",
       x = "", y = "",
       caption = "tdor dataset (Transgender Day of Remembrance 2007-2018)")
```

#### Leading causes of deaths
![](cause.png)
```r
tdor %>%
  filter(Country %in% c("Brazil","Mexico","USA")) %>%
  add_count(`Cause of death`) %>%
  mutate(`Cause of death` = fct_reorder(`Cause of death`, n)) %>%
  group_by(`Cause of death`,Country) %>%
  filter(n > 20, !is.na(`Cause of death`)) %>%
  ggplot(aes(`Cause of death`,n, fill=Country)) +
  geom_col() +
  scale_fill_manual(values = ghibli_palettes$MononokeLight) +
  scale_y_continuous(label = scales::number_format()) +
  coord_flip() +
  labs(title = "Leading Causes of Transgender Deaths",
       subtitle = "In Brazil, Mexico, and USA",
       x= "", y = "",
       caption = "tdor dataset (Transgender Day of Remembrance 2007-2018)")
```

#### Map of Incidents of Transgender Deaths
![](map.png)
```r
tdor %>%
  add_count(Country) %>%
  ggplot(aes(Longitude,Latitude,col = n)) +
  geom_point(alpha=0.4, size=0.5) +
  borders("world", colour = "#455156", size=0.25) +
  scale_color_gradient(low = "lightblue", high = "#EFF1C8", name = "Number of\nDeaths") +
  labs(x="",y="",
       title = "Number of Transgender Deaths Worldwide",
       caption = "tdor dataset (Transgender Day of Remembrance 2007-2018)") +
  theme(plot.background = element_rect(fill = "#263238"),
        panel.background = element_rect(fill = "#263238"),
        panel.grid = element_blank(),
        axis.text = element_blank(),
        axis.ticks = element_blank(),
        legend.background = element_rect(fill = "#354146"),
        text = element_text(color = my_bkgd))

```