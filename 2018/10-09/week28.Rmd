---
title: "US Voter Turnout"
output: html_notebook
---

```{r}
library(tidyverse)
library(wesanderson)

my_font <- "Comfortaa"
my_theme <- theme(text = element_text(family = my_font, color = "#22211d"),
                  rect = element_rect(fill = "#f5f5f2", color = NA), 
                  plot.background = element_rect(fill = "#f5f5f2", color = NA), 
                  panel.background = element_rect(fill = "#f5f5f2", color = NA), 
                  legend.background = element_rect(fill = "#f5f5f2", color = NA),
                  legend.key = element_rect(fill = "#f5f5f2"),
                  plot.caption = element_text(size = 6))

```

```{r}
df <- read_csv("~/R/data/voter_turnout.csv")

```

```{r}
head(df)
```

```{r}
president_years <- seq(1980, 2012, 4)
midterm_years <- seq(1982, 2014, 4)



df <- df %>%
  mutate(election_type = case_when(year %in% president_years ~ "President",
                                   year %in% midterm_years ~ "Midterm")) %>%
  mutate(perc = votes/eligible_voters)

US <- df %>%
  filter(alphanumeric_state_code == 0)

df <- df %>%
  filter(alphanumeric_state_code != 0) 

```

```{r}

bottom <- df %>% 
  group_by(year) %>%
  top_n(-1)

df %>%
  group_by(year) %>%
  ggplot(aes(x=year, y=perc, col=election_type)) + 
  geom_point(alpha=0.5)+ 
  geom_point(data=bottom, col="black", shape=1) +
  geom_text(data=bottom, aes(label=state), angle=270, col="black", size=rel(2.2), nudge_y = -0.02, hjust=0, family=my_font) +
  scale_y_continuous(labels = paste0(seq(0,80,20),"%")) +
  facet_wrap(.~election_type) +
  expand_limits(y = 0) +
  scale_color_manual(values = wes_palettes$Moonrise2) +
  theme_minimal() + my_theme +
  theme(legend.position = "none")+
  labs(title = "Voter Turnout (1980 to 2014)",
       subtitle = "Measured as: votes / eligible voters",
       y="Turnout ", x="",
       caption = "Source: data.world")
   
ggsave("voter_turnout.png")

```

```{r}
df %>%
  filter(election_type=="President") %>%
  group_by(year) %>%
  mutate(perc = votes/eligible_voters) %>%
  top_n(-10,perc) %>%
  group_by(state) %>%
  mutate(count = n()) %>%
  select(state,count) %>%
  unique() %>%
  filter(count > 3) %>%
  ggplot(aes(reorder(state,count),count)) + 
  geom_segment(aes(xend = reorder(state,count), yend=count), col=wes_palettes$Moonrise2[2], y=0, size=2) + 
  geom_point(col=wes_palettes$Moonrise2[2], size=6) +
  coord_flip(ylim = c(0,10)) +
  geom_text(aes(label=count), color = "white", family=my_font) +
  #scale_color_manual(values = wes_palettes$Moonrise2) +
  theme_minimal() + my_theme +
  theme(legend.position = "none",
        axis.text.x = element_blank(),
        axis.ticks = element_blank()) +
  labs(title = "Voter Turnout For Presidential Elections (1980 to 2014)",
       subtitle = "Measured as: votes / eligible voters",
       y="No. of times that State ranks in the bottom 10", x="",
       caption = "Source: data.world")

ggsave("pres_turnout.png")

```

```{r}
df %>%
  filter(election_type=="Midterm") %>%
  group_by(year) %>%
  mutate(perc = votes/eligible_voters) %>%
  top_n(-10,perc) %>%
  group_by(state) %>%
  mutate(count = n()) %>%
  select(state,count) %>%
  unique() %>%
  filter(count > 2) %>%
  ggplot(aes(reorder(state,count),count)) + 
  geom_segment(aes(xend = reorder(state,count), yend=count), col=wes_palettes$Moonrise2[1], y=0, size=2) + 
  geom_point(col=wes_palettes$Moonrise2[1], size=6) +
  coord_flip(ylim = c(0,10)) +
  geom_text(aes(label=count), color = "white", family=my_font) +
  #scale_color_manual(values = wes_palettes$Moonrise2) +
  theme_minimal() + my_theme +
  theme(legend.position = "none",
        axis.text.x = element_blank(),
        axis.ticks = element_blank()) +
  labs(title = "Voter Turnout For Midterm Elections (1980 to 2014)",
       subtitle = "Measured as: votes / eligible voters",
       y="No. of times that State ranks in the bottom 10", x="",
       caption = "Source: data.world")

ggsave("mid_turnout.png")

```


