
# Tidy Tuesday - Week 12
### Hurricanes & Puerto Rico

My contribution to week 12 of TidyTuesday. I started off creating 'copy-cat' area charts and then modified them to reflect cumulative media coverage.

See https://github.com/rfordatascience/tidytuesday for more information.

#### Hurricane Coverage
![](pictures/mc_hurr.png) ![](pictures/mc_hurr_cum.png)

#### State Coverage
![](pictures/mc_states.png) ![](pictures/mc_states_cum.png)

#### Google Searches
![](pictures/ggt.png) ![](pictures/ggt_cum.png)

#### State Mentions vs Mentions including State and ('President' OR 'Trump')
![](pictures/mc_t.png) ![](pictures/mc_t_st_only_cum.png) ![](pictures/mc_t_st_pres_cum.png)


#### Code
![code](pictures/carbon.png)