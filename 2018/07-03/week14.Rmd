---
title: "Tidy Tuesday - Week #14"
subtitle: "Global Life Expectancy"
output: html_notebook
---


```{r}
library(tidyverse)
library(ggthemes)
library(maps)
library(countrycode)

```

```{r}
df <- read_csv("week14_global_life_expectancy.csv")

head(df)
dim(df)
n_distinct(df$country)

```

```{r}

world1 <- sf::st_as_sf(map('world', plot = FALSE, fill = TRUE))

ggplot() + geom_sf(data = world1)


```


```{r}

my_theme <- theme(text = element_text(family = "Comfortaa", color = "#22211d"),
                  rect = element_rect(fill = "#f5f5f2", color = NA), 
                  plot.background = element_rect(fill = "#f5f5f2", color = NA), 
                  panel.background = element_rect(fill = "#f5f5f2", color = NA), 
                  legend.background = element_rect(fill = "#f5f5f2", color = NA),
                  legend.key = element_rect(fill = "#f5f5f2"),
                  plot.caption = element_text(size = 6))

```


```{r}
# reconcile countries in df and world1

df <- df %>% mutate(name = countrycode(df$code,"genc3c","country.name"))

df$name <- str_replace_all(df$name, "St. ", "Saint ")

df$name[df$name == "United States"] <- "USA"
df$name[df$name == "United Kingdom"] <- "UK"
df$name[df$name == "Côte d’Ivoire"] <- "Ivory Coast"
df$name[df$name == "Antigua & Barbuda"] <- "Antigua"
df$name[df$name == "Saint Vincent & Grenadines"] <- "Saint Vincent"
df$name[df$name == "Trinidad & Tobago"] <- "Trinidad"
df$name[df$name == "Micronesia (Federated States of)"] <- "Micronesia"
df$name[df$name == "U.S. Virgin Islands"] <- "Virgin Islands, US"
df$name[df$name == "Congo - Kinshasa"] <- "Democratic Republic of the Congo"

# merge dataframes
df2 <- left_join(df, world1, by=c("name" = "ID"))

```


```{r}

df2 %>%
  filter(year >= 1950) %>% 
  ggplot(aes(x=year)) + geom_bar()

df3 <- df2 %>%
  group_by(name) %>%
  mutate(growth = 100*((life_expectancy / life_expectancy[year == 1950])^(1/(year - 1950))-1)) %>%
  filter(year == 2015) %>% 
  arrange(desc(growth))
  

```



```{r fig.height=3.9, fig.width=9}

df3 %>%
  ggplot(aes(fill=growth)) +
    geom_sf(color = "gray30", size = 0.2) +
    scale_fill_viridis_c(
      name = "CAGR %",
      direction = -1
    ) + 
  labs(title = "Average Annual Change in Life Expectancy",
       subtitle = "1950 to 2015",
       caption = "@DaveBloom11   |  Source: ourworldindata.org") +
  theme_minimal() +
  my_theme +
  theme(axis.text = element_blank())

ggsave("life_expectancy_cagr.png")
```


```{r fig.height=3.9, fig.width=9}

df4 <- df2 %>%
  group_by(name) %>%
  mutate(growth = 100 * ((life_expectancy / life_expectancy[year == 1950] - 1)))%>%
  filter(year == 2015) %>% 
  arrange(desc(growth))
  
df4 %>%
  ggplot(aes(fill=growth)) +
    geom_sf(color = "gray30", size = 0.2) +
    scale_fill_viridis_c(
      name = "Growth %",
      direction = -1
    ) + 
  labs(title = "Total Change in Life Expectancy",
       subtitle = "1950 to 2015",
       caption = "@DaveBloom11   |  Source: ourworldindata.org") +
  theme_minimal() +
  my_theme +
  theme(axis.text = element_blank())

ggsave("life_expectancy_chng.png")



```


```{r fig.height=3.9, fig.width=9}

med_1950 <- median(df2$life_expectancy[df2$year==1950])
med_2015 <- median(df2$life_expectancy[df2$year==2015])

df5 <- df2 %>%
  filter(year == 1950) %>%
  group_by(name) %>%
  mutate(rel_to_median = life_expectancy - med_1950) %>%
  arrange(desc(rel_to_median))
  
df5 %>%
  ggplot(aes(fill=rel_to_median)) +
    geom_sf(color = "gray30", size = 0.2) +
    scale_fill_gradient2_tableau(
      palette = "Red-Blue",
      name = "Age"
    ) + 
  labs(title = "Life Expectancy in 1950",
       subtitle = paste("Relative to Global Median of", format(med_1950, digits=0),"years"),
       caption = "@DaveBloom11   |  Source: ourworldindata.org") +
  theme_minimal() +
  my_theme +
  theme(axis.text = element_blank())

ggsave("life_expectancy_1950.png")



```

```{r fig.height=3.9, fig.width=9}
df6 <- df2 %>%
  filter(year == 2015) %>%
  group_by(name) %>%
  mutate(rel_to_median = life_expectancy - med_2015) %>%
  arrange(desc(rel_to_median))
  
df6 %>%
  ggplot(aes(fill=rel_to_median)) +
    geom_sf(color = "gray30", size = 0.2) +
    scale_fill_gradient2_tableau(
      palette = "Red-Blue",
      name = "Age"
    ) + 
  labs(title = "Life Expectancy in 2015",
       subtitle = paste("Relative to Global Median of", format(as.double(med_2015), digits = 0),"years"),
       caption = "@DaveBloom11   |  Source: ourworldindata.org") +
  theme_minimal() +
  my_theme +
  theme(axis.text = element_blank())

ggsave("life_expectancy_2015.png")

```

