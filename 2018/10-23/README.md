# Tidy Tuesday - Week 30
### Movie Profit

I explored a few areas of the data before deciding on the charts below. I first looked at it annually, but the data is too sporadic before 2000 and sample sizes vary from year to year. I looked at US gross versus WW gross as well as the same data by MPAA rating, but didn't find anything that interested me enough.

In preparing the top 20 chart below I wondered about the $ value of profits and found that the top 10 most profitable horror movies made quite a bit less than the top movies of other genres despite the having the highest return on investment.

I also found that nearly 40% of all dramas lose money compared to 20% of action flicks and 22% for horror films. The sample size varies a lot between genres, but its still an interesting statistic.

See https://github.com/rfordatascience/tidytuesday for more information.

#### Top 20 Most Profitable Movies
![](top20.png)

#### Aggregate Profit Of Top 10 Films In Each Genre
![](agg_profit.png)


#### Number of profitable versus unprofitable films
![](profit_count.png)