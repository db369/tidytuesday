# Tidy Tuesday - Week 36
### Medium Article Metadata

Although the number of articles published falls over the weekend, articles published over the weekend have higher claps -- though Monday has the most. Experimented with tidytext and sentiment analysis as well; almost 2/3 of the articles have net positive sentiment while only 6% are neutral. 

See https://github.com/rfordatascience/tidytuesday for more information.

## Count and Average Claps By Weekday
![](claps_v_count.png)


## Net Sentiment
![](sentiment.png)


