# Tidy Tuesday - Week #10
### Women in the Workforce


See https://github.com/rfordatascience/tidytuesday for more information.

```r
library(tidyverse)
library(ghibli)

jobs_gender <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-03-05/jobs_gender.csv")
earnings_female <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-03-05/earnings_female.csv") 
employed_gender <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-03-05/employed_gender.csv") 

# process jobs_gender data for easy indexing and comparing
processed_jobs_count <- jobs_gender %>%
  select(-total_workers, -total_earnings, -percent_female, -wage_percent_of_male) %>%
  gather(key = "workers_sex", value = "number_of_workers", workers_male:workers_female) %>%
  mutate(workers_sex = str_remove(workers_sex, "workers_"))

processed_jobs_earnings <- jobs_gender %>%
  select(-total_workers, -total_earnings, -percent_female, -wage_percent_of_male) %>%
  gather(key = "workers_sex", value = "total_earnings", total_earnings_male:total_earnings_female) %>%
  mutate(workers_sex = str_remove(workers_sex, "total_earnings_"))

# split major categories into 2 lines for better readability -- used for labeller()
cats <- unique(processed_jobs_count$major_category)
cat_labels <- cats

for (a in 1:length(cats)){
  if(str_length(cats[a]) > 25){
    spaces <- str_locate_all(cats[a],"\\s")[[1]][,1]
    temp <- abs(25 - spaces)
    optimal_space <- spaces[temp == min(temp)]
    ifelse(optimal_space > 0, str_sub(cats[a], optimal_space, optimal_space) <- "\n")
  }
}

male_col = ghibli_palettes$MononokeMedium[3]
female_col = ghibli_palettes$MononokeMedium[6]

processed_jobs_count %>%
  ggplot(aes(year, number_of_workers, fill = workers_sex)) +
  geom_col(position = "dodge") +
  facet_wrap(~major_category) +
  scale_fill_manual(values = c(female_col,male_col), name = "Sex", labels = c("Female","Male")) +
  scale_y_continuous(labels = scales::comma_format()) +
  labs(x = NULL, y = "Number of Workers",
       title = "Number of Female and Male Workers By Major Category") +
  theme(legend.position = c(0.8,0.15)) 

```

![](num_by_cat.png)

```r
processed_jobs_count %>%
  group_by(year, workers_sex, major_category) %>%
  mutate(sum_of_workers = sum(number_of_workers, na.rm = T)) %>%
  group_by(workers_sex, major_category) %>%
  mutate(index = 100 * sum_of_workers / sum_of_workers[year == 2013]) %>%
  ggplot(aes(year, index, col = major_category)) +
  geom_line(aes(linetype = workers_sex), size = 1) +
  scale_color_manual(values = ghibli_palette(8, name ="MononokeMedium", type = "continuous"), guide = F) +
  scale_linetype_manual(values = c(1,2), name = "Sex") +
  facet_wrap(~major_category) +
  labs(x = NULL, y = NULL,
       title = "Number of Female and Male Workers By Major Category",
       subtitle = "Indexed to 100 in 2013") +
  theme(legend.position = c(0.85,0.1)) 

```

![](num_by_cat_index.png)


```r
processed_jobs_earnings %>%
  group_by(year, major_category, workers_sex) %>%
  mutate(median_earnings = median(total_earnings, na.rm = T)) %>%
  ggplot(aes(year, median_earnings, fill = workers_sex)) +
  geom_col(position = "dodge") +
  facet_wrap(~major_category) +
  scale_fill_manual(values = c(female_col,male_col), name = "Sex", labels = c("Female","Male")) +
  scale_y_continuous(labels = scales::comma_format()) +
  labs(x = NULL, y = NULL,
       title = "Median Earnings By Major Category and Sex") +
  theme(legend.position = c(0.8,0.15)) 

```
![](earn_by_cat.png)


```r
processed_jobs_earnings %>%
  group_by(year, workers_sex, major_category) %>%
  mutate(med_earnings = median(total_earnings, na.rm = T)) %>%
  group_by(workers_sex, major_category) %>%
  mutate(index = 100 * med_earnings / med_earnings[year == 2013]) %>%
  ggplot(aes(year, index, col = major_category)) +
  geom_line(aes(linetype = workers_sex), size = 1) +
  scale_color_manual(values = ghibli_palette(8, name ="MononokeMedium", type = "continuous"), guide = F) +
  scale_linetype_manual(values = c(1,2), name = "Sex") +
  facet_wrap(~major_category) +
  labs(x = NULL, y = NULL,
     title = "Median Earnings By Major Category and Sex",
     subtitle = "Indexed to 100 as of 2013") +
  theme(legend.position = c(0.85,0.1)) 


```
![](earn_by_cat_index.png)

```r
earnings_female %>%
  filter(!str_detect(group, "Total,")) %>%
  mutate(group = str_remove(group, " years"),
         group = str_replace(group, " and older", "+")) %>%
  ggplot(aes(Year, percent, col = group)) +
  geom_line(size = 1) + 
  scale_color_manual(values = ghibli_palettes$MononokeMedium,
                     name = "Age") +
  scale_y_continuous(labels = function(x) paste0(x,"%")) +
  labs(x = NULL, y = NULL,
     title = "Women's Earnings as Percent of Men's Earnings",
     subtitle = "By Age") 

ggsave("percent_earn_by_age.png")
```
![](percent_earn_by_age.png)

```r
base_year <- min(earnings_female$Year)

earnings_female %>%
  filter(!str_detect(group, "Total,")) %>%
  mutate(group = str_remove(group, " years"),
         group = str_replace(group, " and older", "+")) %>%
  group_by(group) %>%
  mutate(index = 100 * percent / percent[Year == base_year]) %>%
  ggplot(aes(Year, index, col = group)) +
  geom_line(size = 1) + 
  scale_color_manual(values = ghibli_palettes$MononokeMedium,
                     name = "Age") +
  labs(x = NULL, y = NULL,
     title = "Growth in Women's Earnings as Percent of Men's Earnings",
     subtitle = paste0("By Age Group, Indexed to 100 in ", base_year)) +
  theme(legend.position = c(0.3,0.9),
        legend.direction = "horizontal",
        legend.background = element_blank(),
        plot.margin = margin(10,60,10,10)) +
  annotate("text", label = "◄ 25 - 54", x = 2012, y = 135, shape = 25, color = "grey20", 
           fill = "grey20", size = 4, hjust = 0, family = my_font) +
  annotate("text", label = "◄ 20 - 54,\n    55 - 64", x = 2012, y = 121, shape = 25, color = "grey20", 
           fill = "grey20", size = 4, hjust = 0, family = my_font) +
  annotate("text", label = "◄ 16 - 19,\n    65+", x = 2012, y = 104, shape = 25, color = "grey20", 
           fill = "grey20", size = 4, hjust = 0, family = my_font) +
  coord_cartesian(clip = "off")
```
![](percent_earn_by_age_index.png)

```r
full_time <- employed_gender %>%
  select(year, full_time_male, full_time_female) %>%
  gather(key = "sex", value = "percent", full_time_female:full_time_male) %>%
  mutate(sex = str_remove(sex, "full_time_"))
  
full_time %>%
  ggplot(aes(year, percent, col = sex)) +
  geom_line(size = 1) +
  geom_smooth(show.legend = F) +
  scale_y_continuous(labels = function(x) paste0(x,"%"))+
  scale_color_manual(values = ghibli_palettes$MononokeMedium, name = "Sex") + 
  labs(x = NULL, y = NULL,
       title = "Percent of employed men and women usually working full time") 

```
![](full_time.png)


```r
part_time <- employed_gender %>%
  select(year, part_time_male, part_time_female) %>%
  gather(key = "sex", value = "percent", part_time_female:part_time_male) %>%
  mutate(sex = str_remove(sex, "part_time_"))
  
part_time %>%
  ggplot(aes(year, percent, col = sex)) +
  geom_point(size = 1, alpha = 0.6) +
  geom_smooth(show.legend = F) +
  scale_y_continuous(labels = function(x) paste0(x,"%"))+
  scale_color_manual(values = ghibli_palettes$MononokeMedium, name = "Sex") +
  labs(x = NULL, y = NULL,
       title = "Percent of employed men and women usually working part time") 

```
![](part_time.png)


```r
full_time %>%
  ggplot(aes(year, percent, col = sex), size = 1) +
  geom_point(size = 1, alpha = 0.6) +
  geom_line(data = part_time, aes(year, percent, col = sex), linetype = 2, size = 1) +
  annotate("point", shape = 24, x = 1980, y = 70, show.legend = F, color = "grey20", fill = "grey20", size = 2) +
  annotate("point", shape = 25, x = 2000, y = 28, show.legend = F, color = "grey20", fill = "grey20", size = 2) +
  annotate("text", x = 1980, y = 65, label = "Full Time", 
            family = my_font, col = "grey20") +
  annotate("text", x = 2000, y = 33, label = "Part Time", 
            family = my_font, col = "grey20") +
  scale_y_continuous(labels = function(x) paste0(x,"%")) +
  scale_color_manual(values = ghibli_palettes$MononokeMedium, name = "Sex") +
  labs(x = NULL, y = NULL,
       title = "Percent of employed men and women usually working full/part time") 

  
```
![](full_and_part_time.png)