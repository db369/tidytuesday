# Tidy Tuesday - Week #7
### Federal Research and Development Spending by Agency


See https://github.com/rfordatascience/tidytuesday for more information.

![](gcc_spending.png)

# Note: I added second version using a log scale for the y-axis
![](gcc_spending_log.png)

![](gcc_spend_chg.png)

![](gcc_cummean.png)

## Code Snippet
```r

# load libraries and set up theme
library(tidyverse)
library(ghibli)
library(lubridate)

my_font <- "Comfortaa"
my_bkgd <- "#f5f5f2"
my_theme <- theme(text = element_text(family = my_font, color = "#22211d"),
                  rect = element_rect(fill = my_bkgd),
                  plot.background = element_rect(fill = my_bkgd, color = NA),
                  panel.background = element_rect(fill = my_bkgd, color = NA),
                  panel.border = element_blank(),
                  legend.background = element_rect(fill = my_bkgd, color = NA),
                  legend.key = element_rect(fill = my_bkgd),
                  plot.caption = element_text(size = 6))

theme_set(theme_light() + my_theme)

# load data
climate_spend <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-02-12/climate_spending.csv")

# process and build line chart
climate_spend %>%
  mutate(gcc_spending = gcc_spending / 1e6) %>%
  ggplot(aes(year, gcc_spending, group = department, col = department)) +
  geom_line(show.legend = F, size = 1.5) +
  geom_text(aes(label = ifelse(year == max(year), department, ''), x = max(year), y = gcc_spending + 
                  case_when(department == "NSF" ~ 70,
                            department == "All Other" ~ -60,
                            department == "Agriculture" ~ 30,
                            TRUE ~ 0)),
                           show.legend = F, hjust=0, nudge_x = 0.5, family = my_font) +
  scale_color_manual(values = ghibli_palettes$MarnieMedium2) +
  scale_y_continuous(labels = scales::dollar_format()) +
  theme(plot.margin = margin(5,90,5,5)) +
  labs(x = "", y = "",
       title = "Global Climate Change Spending",
       subtitle = "In $ millions",
       caption = "Source: American Association for the Advancement of Science") +
  coord_cartesian(clip = 'off')
  
# build chart indexing change to 100
climate_spend %>%
  mutate(gcc_spending = gcc_spending / 1e6,
         change = gcc_spending / gcc_spending[year == 2000] * 100) %>%
  ggplot(aes(year, change, group = department, col = department)) +
  geom_line(show.legend = F, size = 1.5) +
  scale_color_manual(values = ghibli_palettes$MarnieMedium2) +
  geom_text(aes(label = ifelse(year == max(year), 
                               paste0(department,
                                      ifelse(department == "Commerce (NOAA)","\n","")," (", round(change), ")"), ''), 
                x = max(year), y = change + 
                  case_when(department == "NSF" ~ -5,
                            department == "Agriculture" ~ 20,
                            department == "Interior" ~ 40,
                            TRUE ~ 0)),
                           show.legend = F, hjust=0, nudge_x = 0.5, family = my_font) +
  theme(plot.margin = margin(5,90,5,5)) +
  labs(x = "", y = "",
       title = "Change in Global Climate Change Spending",
       subtitle = "Indexed to 100 at Y2000",
       caption = "Source: American Association for the Advancement of Science") +
  coord_cartesian(clip = 'off')

# build area chart reflecting cumulative mean spend
climate_spend %>%
  #filter(department != "NASA") %>%
  group_by(department) %>%
  mutate(gcc_spending = gcc_spending / 1e6,
         cummean_spend = cummean(gcc_spending)) %>%
  ggplot(aes(year, cummean_spend, fill = department)) +
  geom_area(show.legend = F, alpha = 0.4) +
  geom_line(show.legend = F, aes(col = department)) +
  scale_color_manual(values = ghibli_palettes$MarnieMedium2) +
  scale_fill_manual(values = ghibli_palettes$MarnieMedium2) +
  scale_y_continuous(labels = scales::dollar_format()) +
  facet_wrap(~department, scales = "free_y") + 
  labs(x = "", y = "$ Millions",
       title = "Cumulative Average GCC Spend",
       subtitle = "By Department",
       caption = "Source: American Association for the Advancement of Science") +
  theme(strip.background = element_rect(fill = my_bkgd),
        strip.text = element_text(hjust = 1, color = "#1C271D", size = 12))


```
