# Tidy Tuesday - Week #15
### Tennis Grand Slams


See https://github.com/rfordatascience/tidytuesday for more information.



## Code Snippet
```r
library(tidyverse)
library(rcartocolor)

player_dob <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-04-09/player_dob.csv")
grand_slams <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-04-09/grand_slams.csv")


player_dob %>%
  filter(!is.na(age)) %>%
  mutate(age_yrs = age / 365,
         age_buckets = paste0(4 * (age_yrs %/% 4), " to ", 4 * (age_yrs %/% 4) + 4)) %>%
  ggplot(aes(date_of_birth,date_of_first_title, fill=age_buckets)) +
  geom_point(shape = 22, col = "gray20", size = 5) +
  scale_fill_carto_d(palette = "Earth", name = "Player\nAge") +
  labs(x = "Date of Birth", y = "Date of First Title",
       title = "Tennis Player Age at First Title Relative to Date of Birth",
       caption = "Source: Wikipedia  |  By @DaveBloom11") +
  theme(plot.background = element_rect(fill = "gray10"),
        panel.background = element_rect(fill = "gray20"),
        legend.background = element_rect(fill = "gray10"),
        legend.key = element_rect(fill = "gray10"),
        legend.text = element_text(color = my_bkgd),
        panel.grid = element_line(color = "gray10"),
        title = element_text(color = my_bkgd))


```
![](first_title_age.png)

```r
grand_slams %>%
  mutate(grand_slam = str_to_title(str_replace(grand_slam, "_", " ")),
         grand_slam = str_replace(grand_slam, "Us ", "US ")) %>%
  group_by(name) %>%
  mutate(total_wins = n()) %>%
  ungroup() %>%
  mutate(name = paste0(name, " - ",total_wins, " wins")) %>%
  filter(total_wins > 5) %>%
  mutate(name = fct_reorder(name, tournament_date)) %>%
  ggplot(aes(name, tournament_date)) +
  geom_line(col = "gray40") +
  geom_point(shape = "▌", aes(col = grand_slam), size = 2) +
  coord_flip() +
  scale_color_carto_d(palette = "Earth", name = "Grand Slam\nTournament") +
  facet_wrap(~gender, nrow = 2, scales = "free_y") +
  labs(x = "", y = "Date of Tournament",
       title = "Timeline of Grand Slam Wins",
       subtitle = "For players with over 5 wins",
       caption = "Source: Wikipedia  |  By @DaveBloom11") +
  theme(plot.title = element_text(hjust = -100),
        plot.subtitle = element_text(hjust = -3),
        plot.caption = element_text(hjust = 2),
        panel.border = element_rect(color = "gray90", fill=NA))

```
![](timeline.png)