# Tidy Tuesday - Week #14
### Seattle Bike Counters


See https://github.com/rfordatascience/tidytuesday for more information.

```r
library(tidyverse)
library(lubridate)
library(ghibli)

bike_traffic_raw <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-04-02/bike_traffic.csv", 
                             col_types = "cccdd")

bike_traffic <- bike_traffic_raw %>% mutate(date = mdy_hms(date))

bike_traffic %>%
  mutate(dow = wday(date, label = T)) %>%
  group_by(yr = factor(year(date)), dow) %>%
  filter(!yr %in% c(2013,2019),
         crossing != 'Sealth Trail') %>%
  summarize(bikes = sum(bike_count, na.rm = T), peds = sum(ped_count, na.rm = T)) %>%
  ggplot(aes(dow, bikes, group = yr, col = yr)) +
  geom_line(size = 2) +
  geom_point(shape = 21, fill = my_bkgd, size = 4, show.legend = F) +
  scale_color_manual(values = ghibli_palettes$PonyoMedium, name = "Year") +
  scale_y_continuous(labels = function(x) x/1000) +
  labs(title = "The Seattle bike ridership base appears to be declining annually",
       x="Day of Week", y="Number of Bikes (in thousands)",
       caption = "Source: Seattle Department of Transportation | By @DaveBloom11") +
  theme(panel.border = element_rect(color = "gray90", fill = NA))

```
![](bike_by_dow.png)

```r
bike_traffic %>%
  mutate(hr = hour(date),
         dow = wday(date, label = T)) %>%
  group_by(yr = year(date), hr) %>%
  filter(!yr %in% c(2013,2019),
         crossing != 'Sealth Trail') %>%
  summarize(bikes = sum(bike_count, na.rm = T), peds = sum(ped_count, na.rm = T)) %>% 
  ggplot(aes(hr, bikes, group = yr, col = factor(yr))) +
  geom_line(size = 1) +
  geom_vline(xintercept = c(8,17), linetype = 2) +
  scale_color_manual(values = ghibli_palettes$PonyoMedium, name = "Year") +
  scale_y_continuous(labels = function(x) x/1000) +
  scale_x_continuous(breaks = seq(0,23)) +
  annotate("text", label = "Peak Commuting\nTimes", family = my_font, x = 12.5, y = 25000) +
  geom_segment(xend = 8.5, x = 11, y = 20000, yend = 20000, col = "gray30", arrow = arrow(angle = 30, length = unit(2, "mm"))) +
  geom_segment(xend = 16.5, x = 14, y = 20000, yend = 20000, col = "gray30", arrow = arrow(angle = 30, length = unit(2, "mm"))) +
  labs(x = "Hour of Day", y = "Number of Bikes Counted (in thousands)",
       title = "Number of Bikes Counted By Hour of Day and Year",
       caption = "Source: Seattle Department of Transportation | By @DaveBloom11") +
  theme(panel.border = element_rect(color = "gray90", fill = NA),
        legend.position = c(0.03,0.97),
        legend.justification = c(0,1),
        legend.background = element_rect(color = "gray90"))

```
![](bikes_by_hour.png)

```r
bike_traffic %>%
  filter(crossing != "Sealth Trail") %>%
  mutate(hr = hour(date),
         dow = wday(date, label = T)) %>%
  group_by(hr, dow, crossing) %>%
  summarize(bikes = sum(bike_count, na.rm = T), peds = sum(ped_count, na.rm = T)) %>% 
  mutate(mbikes = median(bikes, na.rm = T)) %>%
  ggplot(aes(hr, fct_rev(dow))) +
  geom_tile(aes(fill = bikes)) +
  facet_wrap(~crossing) +
  scale_fill_viridis_c(option = "magma", guide = guide_legend(title.position = 'top', label.position = 'bottom', direction = 'horizontal',
                                                              reverse = T, title = "Number of Bikes", keywidth = unit(10,"mm"),
                                                              keyheight = unit(1,"mm"), label.hjust = 0,
                                                              label.theme = element_text(size = 7, family = my_font),
                                                              title.theme = element_text(size = 8, family = my_font)),
                       labels = scales::comma_format()) +
  scale_x_continuous(breaks = seq(0,23,2)) +
  labs(x = "Hour of Day", y = "Day of Week",
       title = "Peak Hours for Seattle Cyclists By Crossing",
       caption = "Source: Seattle Department of Transportation | By @DaveBloom11") +
  theme(legend.position = c(0,-0.16),
        legend.justification = 'left',
        legend.background = element_blank(),
        plot.margin = margin(5,5,20,5),
        strip.text = element_text(size = 8))


```
![](heatmap.png)