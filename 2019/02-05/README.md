# Tidy Tuesday
### House and Mortgage data



See https://github.com/rfordatascience/tidytuesday for more information.

![](rates_recessions.png)

## Code Snippet
```r
library(tidyverse)
library(lubridate)
library(ghibli)
library(readxl)

my_font <- "Comfortaa"
my_bkgd <- "#f5f5f2"
my_theme <- theme(text = element_text(family = my_font, color = "#22211d"),
                  rect = element_rect(fill = my_bkgd),
                  plot.background = element_rect(fill = my_bkgd, color = NA),
                  panel.background = element_rect(fill = my_bkgd, color = NA),
                  panel.border = element_blank(),
                  legend.background = element_rect(fill = my_bkgd, color = NA),
                  legend.key = element_rect(fill = my_bkgd),
                  plot.caption = element_text(size = 6))

theme_set(theme_light() + my_theme)

# read in data
recession_dates <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-02-05/recessions.csv")

# I couldn't read the mortgage csv properly so I pulled data from the xls the same way described
# on the project page
mortgage_rates <- read_xls("../data/historicalweeklydata.xls", skip = 6, sheet = 2,
                  col_types = "numeric") %>%
  set_names(nm = c("date", "fixed_rate_30_yr", "fees_and_pts_30_yr",
                   "fixed_rate_15_yr",
                   "fees_and_pts_15_yr", "adjustable_rate_5_1_hybrid",
                   "fees_and_pts_5_1_hybrid", "adjustable_margin_5_1_hybrid",
                   "spread_30_yr_and_fixed_5_1_adjustable", "delete")) %>%
  select(-delete) %>%
  mutate(date = as.Date(date, origin = "1899-12-30"))

gathered_mortgage_rates <- mortgage_rates %>%
  gather(key = "type", value = "rate", -date)

# Clean recession data
pattern <- "\\d+([a-zA-Z{3,5}]+).(\\d+)\\-([a-zA-Z{3,5}]+).(\\d+)"

rec_dates_proc <- recession_dates %>%
  mutate(start_date = dmy(paste(1, str_match(period_range, pattern)[,2], str_match(period_range, pattern)[,3],sep="-")),
         end_date = dmy(paste(1, str_match(period_range, pattern)[,4], str_match(period_range, pattern)[,5],sep="-"))) %>%
  select(name, start_date, end_date)

rec_dates_proc$start_date[rec_dates_proc$name == "Great Depression"] = dmy("1-Oct-1929")

rec_dates_proc <- rec_dates_proc %>%
  mutate(mid_point = start_date + floor(end_date - start_date)/2)

# filter recession dates so they align with mortgage rate data
rec_dates_filtered <-  rec_dates_proc %>% filter(year(start_date) > 1970)

# I decided to use less data than provided
select_rates <- c("fixed_rate_30_yr", "fixed_rate_15_yr", "adjustable_rate_5_1_hybrid")
custom_labels <- str_to_title(str_replace_all(select_rates,"_"," "))

# build plot
rec_dates_filtered <-  rec_dates_proc %>% filter(year(start_date) > 1970)

gathered_mortgage_rates %>%
  filter(type %in% select_rates) %>%
  ggplot() +
  geom_line(aes(x = date, y = rate, group = type, col = type, linetype = type), na.rm = T) +
  geom_rect(aes(xmin = start_date, xmax = end_date, ymin = 0, ymax = 19, fill = name), 
            col = my_bkgd, alpha = 0.2, show.legend = F, data = rec_dates_filtered) +
  ggrepel::geom_label_repel(aes(label = name, x = mid_point, y = c(3,5,6,17,13,9)), alpha = 0.6, min.segment.length = 0,
                            family = my_font, show.legend = F, data = rec_dates_filtered) + 
  scale_y_continuous(labels = function(x) paste0(x,"%")) +
  scale_colour_manual(values = ghibli_palettes$KikiMedium, name = "Mortgage Type", labels=custom_labels) +
  scale_linetype_manual(values = c(1:3), name = "Mortgage Type", labels=custom_labels) +
  scale_fill_manual(values = ghibli_palettes$KikiMedium) +
  labs(x = "", y = "Mortgage Rate",
       title = "Mortgage Rates vs Economic Recessions",
       caption = "Sources: FreddieMac, Wikipedia") +
  theme(legend.position = "top")


```
