# TidyTuesday
## A weekly data project in R from the R4DS online learning community 
#### See https://github.com/rfordatascience/tidytuesday for more information.

### Below are some of my contributions to TidyTuesday, all of which can be found in the subfolders as well as in my twitter profile, <a href="https://twitter.com/DaveBloom11">@DaveBloom11</a>. 

## <a href= "2020/01-21/">Hotel Bookings</a>
![](2020/01-21/hotel_bookings.png)
## <a href= "2020/01-14/">Passwords</a>
![](2020/01-14/password_strength.png)
## <a href= "2020/01-07/">Australian Fires</a>
![](2020/01-07/avg_monthly_temps.png)
## <a href= "2019/11-05/">Bike & Walk Commutes</a>
![](2019/11-05/BikeOrWalk_reduced.png)
## <a href= "2019/04-09/">Tennis Grand Slam Champions</a>
![](2019/04-09/first_title_age.png)
## <a href= "2019/04-02/">Seattle Bike Traffic</a>
![](2019/04-02/heatmap.png)
![](2019/04-02/bike_by_dow.png)
## <a href= "2019/03-19/">Stanford Open Policing Project</a>
![](2019/03-19/map.png)
## <a href= "2019/02-26/">French Train Delays</a>
![](2019/02-26/trips_by_route.png)
![](2019/02-26/madrid_marseille.png)
## <a href= "2019/02-19/">US PhD's Awarded</a>
![](2019/02-19/top4.png)
![](2019/02-19/broad_field.png)
## <a href= "2019/02-12/">Federal R&D Spending</a>
![](2019/02-12/gcc_spend_chg.png)
## <a href= "2019/02-05/">House Price Index & Mortgage Rates</a>
![](2019/02-05/rates_recessions.png)
## <a href= "2018/11-27/">Baltimore Bridges</a>
![](2018/11-27/map.png)
## <a href= "2018/11-20/">Thanksgiving Dinner or Transgender Day of Remembrance</a>
![](2018/11-20/map.png)
## <a href= "2018/10-23/">Horror Movie Profit</a>
![](2018/10-23/top20.png)
## <a href= "2018/10-02/">US Births</a>
![](2018/10-02/us_births.png)
## <a href= "2018/09-25/">Global Invasive Species</a>
![](2018/09-25/InvCost_GDPM.png)
## <a href= "2018/07-03/">Global Life Expectancy</a>
![](2018/07-03/life_expectancy_1950.png)
## <a href= "2018/06-26/">Alcohol Consumption</a>
![](2018/06-26/bev.png)
## <a href= "2018/06-19/">Hurricanes & Puerto Rico</a>
![](2018/06-19/pictures/tv_hurr.png)
  
